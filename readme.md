How does this repo work? Not even I know.

# Todo

- [x] Create a plank like block of relevant proportions.
- [x] Create several of them, with random poses.
- [x] Have a depth camera and logic camera look at the objects, and produce data.
- [ ] Create a realistic world, with distracting objects.
- [x] Have the camera move around randomly.
- [ ] Extract data automatically.
- [ ] Script and automate this process, to generate a dataset.

# Build

```
docker build -t gazebo .
```

# Run

```
docker run -it \
-e DISPLAY=$DISPLAY \
-v /tmp/.X11-unix:/tmp/.X11-unix:ro \
gazebo
```

# Other

In order to prevent having to re-download every single model for each docker
container, the `download_models.sh` script is included to allow you to download
a copy of the entire public gazebo model database in such a way that they will
be transferred to each new docker container created.

It will download the models specified in the `extra_models.txt` file.

Run it like this:

```
./download_models.sh
```
# Issues


