FROM ubuntu:16.04

RUN apt-get update
RUN apt-get install curl -y
RUN curl -sSL http://get.gazebosim.org | sh
RUN apt-get install libgazebo9-dev -y

ENV GAZEBO_PLUGIN_PATH=/home/plugins/build
ENV GAZEBO_MODEL_PATH=/home/models:/home/standard_models

ADD extra_models.txt /home/extra_models.txt
ADD include /home

WORKDIR /home/plugins/build
RUN cmake ../ && make

WORKDIR /home

CMD gazebo --verbose lit_world.world
