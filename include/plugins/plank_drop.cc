#include <ignition/math/Pose3.hh>
#include "gazebo/physics/physics.hh"
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"
#include <iostream>
#include <string>
#include <fstream>
#include <streambuf>
#include <random>


//////////////////////////////////////////////////////////////

// Generates a double within the range low and high
double rand_range(double low, double high)
{
    double random_in_range = low +
       static_cast <double> (rand()) /
       (static_cast <double> ( RAND_MAX / (high - low))
    );

    return random_in_range;
}

// Creates pose data for planks
double drop_cord_neg () {return rand_range(-1.0, 1.0);}
double drop_cord_pos () {return rand_range(0.0, 4.0);}

// Creates position data for the cam
// z: above ground level
double cam_pos_z () {return rand_range(1.0, 7.0);}

// x,y: More or less anywhere
double cam_pos_xy () {return rand_range(-10.0, 10.0);}

// roll: not enough so the camera is upside down, but permuted
//       enough
double cam_roll () {return rand_range(-2.0, 2.0);}

// pitch: usually pointing at the ground, but not always
double cam_pitch () {return rand_range(-0.5, 2.0);}

// yaw: oriented based on which quadrant the camera is in
//      to point mostly towards 0,0,0
double cam_yaw (double cam_x, double cam_y)
{
    double range_low;
    double range_high;
    
    if (cam_x < 0 && cam_y < 0)
    {
        range_low = 0.0;
        range_high = 1.570796327;
    }
    else if (cam_x > 0 && cam_y < 0)
    {
        range_low = 1.570796327;
        range_high = 3.141592654;
    }
    else if (cam_x > 0 && cam_y > 0)
    {
        range_low = 3.141592654;
        range_high = 4.71238898;
    }
    else
    {
        range_low = 4.71238898;
        range_high = 6.283185307;
    }

    return rand_range(range_low, range_high);
}

// Joins a list of coordinates together, so they can be specified for the SDF pose
std::string join_cords (std::list<double> cords)
{
    std::stringstream joined;

    for (auto iter = cords.begin(); iter != cords.end(); iter++)
    {
        if (iter != cords.begin())
            joined << " ";
        joined << std::to_string(*iter);
    }

    return joined.str();
}

// Inserts a model with a random pose in the center of the scene.
sdf::SDFPtr drop_model (std::string model_name, std::string name_suffix)
{
    // Get the path to the model
    std::string model_file_path = gazebo::common::ModelDatabase::Instance()->GetModelFile("model://" + model_name);

    // Create an SDF object from a string
    sdf::SDFPtr modelSDF = sdf::readFile(model_file_path);

    // Get a pointer to the model element
    sdf::ElementPtr model_elm = modelSDF->Root()->GetElement("model");

    // Set the model name to be unique
    model_elm->GetAttribute("name")->SetFromString(model_name + name_suffix);

    // Set the model's pose to a random value
    std::string model_pose_string = join_cords({
            drop_cord_neg(), drop_cord_neg(), drop_cord_pos(), 
            drop_cord_neg(), drop_cord_neg(), drop_cord_neg()
            });
    model_elm->GetElement("pose")->GetValue()->Set(model_pose_string);

    // Return the model
    return modelSDF;
}

//////////////////////////////////////////////////////////////
namespace gazebo
{
class PlankDrop : public WorldPlugin
{
    public: void Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/)
    {
        // Seed randomness
        srand(static_cast <unsigned> (time(0)));

        // Adding planks
        for (int x = 0; x <= 10; x++)
        {
            // Generate the plank model in a pose ready for dropping
            sdf::SDFPtr plankSDF = drop_model("plank", std::to_string(x));

            // Insert the model
            _parent->InsertModelSDF(*plankSDF);
        }
        
        // Adding extra models
        // Get all acceptable extra models
        std::ifstream extramodel_file("extra_models.txt");
        std::vector<std::string> extramodel_names;

        std::string line;
        while (std::getline(extramodel_file, line))
        {
            extramodel_names.push_back(line);
        }
        
        // Insert extra models
        for (int x = 0; x <= 15; x++)
        {
            // Select a random model to use
            int rand_index = int(rand_range(0, extramodel_names.size()));
            std::string rand_model_name = extramodel_names[rand_index];

            // Generate the plank model in a pose ready for dropping
            sdf::SDFPtr extraSDF = drop_model(rand_model_name, std::to_string(x));

            // Insert the model
            _parent->InsertModelSDF(*extraSDF);
        }

        // Adding the camera
        // Insert the multi output camera
        std::string cam_file_path = common::ModelDatabase::Instance()->GetModelFile("model://multi-cam");
        sdf::SDFPtr camSDF = sdf::readFile(cam_file_path);
        sdf::ElementPtr cam = camSDF->Root()->GetElement("model");

        // Set the model's pose to random-ish values
        double cam_x = cam_pos_xy();
        double cam_y = cam_pos_xy();
        double set_cam_yaw = cam_yaw(cam_x, cam_y);
        std::string cam_pose_string = join_cords({
                cam_x, cam_y, cam_pos_z(),
                cam_roll(), cam_pitch(), set_cam_yaw
                });
        cam->GetElement("pose")->GetValue()->Set(cam_pose_string);
 
        // Insert the camera
        _parent->InsertModelSDF(*camSDF);
    }
};


GZ_REGISTER_WORLD_PLUGIN(PlankDrop )
}

